using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedController : MonoBehaviour
{
    public float jumpforce = 40;
    public int enemyDodge = 0;
    private const string enemy = "Enemigo";
    private Rigidbody2D rb;
    private Animator animator;
    public float speed = 6;

    
    private const string head_object = "Cabeza";
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetInteger("State", 0);
        rb.velocity = new Vector2(speed, rb.velocity.y);
        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * jumpforce, ForceMode2D.Impulse);
            animator.SetInteger("State", 1); 
        }
        if (Input.GetKey(KeyCode.X))
        {
            animator.SetInteger("State", 2); 
        }
        if (enemyDodge >= 10)
        {
            animator.SetInteger("State", 3);
            
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
        
    }
     private void OnCollisionEnter2D(Collision2D collision)
    {
        
        
        if (collision.gameObject.CompareTag(enemy) && Input.GetKey(KeyCode.X))
        {
            
            Destroy(collision.gameObject);
            enemyDodge = enemyDodge + 1;
            speed = speed + 1.5f;
        }else
        {
             if (collision.gameObject.CompareTag(enemy))
            {
                Destroy(this.gameObject);
            }
        
        }
       

    }
     private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag(head_object))
        {
            enemyDodge = enemyDodge + 1;
            speed = speed + 1.5f;
        }
    }
}
